using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{

    public bool occupied;

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Enemy"))
        {
            occupied = true;
            Debug.Log("OCCUPIED TRUE");
        }        
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.CompareTag("Enemy"))
        {
            Invoke("SetOccupiedFalse", 1);
            Debug.Log("OCCUPIED FALSE");
        }
    }

    private void SetOccupiedFalse()
    {
        occupied = false;
        Debug.Log("OCCUPIED SET FALSE");
    }
}
