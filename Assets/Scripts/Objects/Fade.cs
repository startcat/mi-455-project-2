using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour
{
    private float timeStart = 0;
    private float fadeTime = 2;
    private Color color;
    // Start is called before the first frame update
    void Start()
    {
        Material mat = gameObject.GetComponent<Renderer>().material;
        Shader shader = Resources.Load<Shader>("DitherBasic");
        color = mat.color;
        mat = new Material(shader);
        mat.color = color;
        gameObject.GetComponent<Renderer>().material = mat;
        timeStart = Time.time;

    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time - timeStart < fadeTime)
        {
            color.a = (Time.time - timeStart) / fadeTime;
            GetComponent<Renderer>().material.SetFloat("_Opacity", 1-color.a);
        }
        else{
            Destroy(gameObject);
        }
    }
}
