using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectHealth : MonoBehaviour
{
    [Header("Health of the object")]
    public float health = 1;

    [Header("Select How the object will change (use none if it doesn't)")]
    public DestroyMode destroyMode = DestroyMode.None;

    [Header("For objects like the water cooler or vent, put the object prefab here")]
    public GameObject stateChangePrefab;

    //gameobjects for switching the mesh
    private GameObject broken1;
    private GameObject broken2;

    [Range(1,100)]public int breakScore;

    /// <summary>
    /// Meant to be called by Cats code that's dealing with launching the items
    /// </summary>
    /// <param name="damage">The amount of damage the object is taking</param>
    public void Damage(float damage)
    {
        Debug.Log("DAMAGE CALLED");
        health -= damage;
        if(health <= 0)
        {
            if (destroyMode == DestroyMode.Shatter)
            {
                
                Invoke("Shatter", Time.fixedDeltaTime);
            }
            else if(destroyMode == DestroyMode.StateChange)
            {
                StateChange();
            }
            else
            {
                //Maybe we want a cracked material on it if its damaged but won't break, we would do that here
            }

            GM.instantPoints(breakScore * 100);
        }
    }

    /// <summary>
    /// Object will shatter into like a billion pieces (estimation)
    /// </summary>
    private void Shatter()
    {
        float multMin= 0.5f;
        float multMax= 1.5f;
        //Make Object shatter here
        //get the two children of the object that is breaking
        broken1 = this.transform.GetChild(0).gameObject;//unbroken object
        broken2 = this.transform.GetChild(1).gameObject;//broken object
        //set the position and rotation of the broken object to the pos/rot of the unbroken one
        broken2.transform.position = broken1.transform.position;
        broken2.transform.rotation = broken1.transform.rotation;
        Vector3 velocity = broken1.GetComponent<Rigidbody>().velocity;
        //deactivate the unbroken object and activate the broken one
        broken1.SetActive(false);
        broken2.SetActive(true);

        foreach (Rigidbody rb in broken2.GetComponentsInChildren<Rigidbody>())
        {
            rb.velocity = new Vector3(velocity.x * Random.Range(multMin,multMax), velocity.y * Random.Range(multMin, multMax), velocity.z * Random.Range(multMin, multMax));

            rb.gameObject.AddComponent<Fade>();
        }
        //Debug.Log("Shatter()");
    }


    /// <summary>
    /// This is for things like a water cooler or vent where when you hit it it changes into something different that has alternative functionaility
    /// </summary>
    private void StateChange()
    {
        
        Instantiate(stateChangePrefab, transform.position + stateChangePrefab.transform.position, stateChangePrefab.transform.localRotation);
        Destroy(gameObject);
    }


    private void Breaking()
    {
        //move the cooler breaking her probably
    }

    
}
public enum DestroyMode
{
    Shatter,
    StateChange,
    None
}
