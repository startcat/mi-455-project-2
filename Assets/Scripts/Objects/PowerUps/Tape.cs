using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tape : MonoBehaviour
{
    [Range(0, 20)]
    public int activeTime = 10;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(TapeActive(other));
            gameObject.transform.position = new Vector3(0, -10000, 0);


        }
    }

    public IEnumerator TapeActive(Collider collision)
    {
        Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
        GameObject GO = collision.gameObject;
        float timer = Time.time;
        float massStorage = rb.mass;
        Vector3 scaleStorage = GO.transform.localScale;
        Debug.Log(scaleStorage);

        while (activeTime > Time.time-timer)
        {
            collision.gameObject.transform.localScale = new Vector3(scaleStorage.x * (1+(Time.time - timer) / activeTime), scaleStorage.y * (1+(Time.time - timer) / activeTime), scaleStorage.z * (1+(Time.time - timer) / activeTime));
            collision.gameObject.GetComponent<Rigidbody>().mass = massStorage * (1+(Time.time-timer)/(activeTime/2));
            Debug.Log(Time.time-timer);
            yield return new WaitForFixedUpdate();
        }
        collision.gameObject.transform.localScale = scaleStorage;
        collision.gameObject.GetComponent<Rigidbody>().mass = massStorage;
        Destroy(gameObject);
        yield return null;
    }
}
