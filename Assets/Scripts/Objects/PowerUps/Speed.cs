using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed : MonoBehaviour
{
    [Range(1f, 10f)] public float speedMultiplier = 1f;

    [Range(0f, 20f)] public float activeTime = 3f;
    [Range(0f, 10f)] public float speedFallOffTime = 1f;

    public bool refillStamina = false;
    public bool freezeStamina = false;
    public bool respawn = false;
    public float resawnTime = 10f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            var roller = other.gameObject.GetComponent<Roll>();

            roller.powerUpDecay = speedFallOffTime;
            roller.powerUpDecayTime = speedFallOffTime;
            roller.powerUpActiveTime = activeTime;
            roller.powerUpSpeedMulti = speedMultiplier;

            if (refillStamina) { roller.sprintTimeCurrent = roller.sprintTimeTotal; }
            if (freezeStamina) { StartCoroutine(LockStamina(roller)); }
            else if (respawn) { StartCoroutine(RespawnTimer()); }
            else { Destroy(gameObject); }
        }
    }

    public IEnumerator LockStamina(Roll roller)
    {
        roller.staminaLocked = true;
        gameObject.transform.position = new Vector3(0, -10000, 0);
        yield return new WaitForSeconds(activeTime);
        roller.staminaLocked = false;
        Destroy(gameObject);
    }

    public IEnumerator RespawnTimer()
    {
        var startPos = transform.position;
        gameObject.transform.position = new Vector3(0, -10000, 0);
        yield return new WaitForSeconds(resawnTime);
        gameObject.transform.position = startPos;
    }
}
