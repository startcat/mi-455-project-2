using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoints : MonoBehaviour
{
    public float pointMultiplier = 1f;
    public float minMoveDistance = 1f;
    public float maxPoints = 100f;

    public float minStopSpeed = 1f;

    private Rigidbody myBod;
    private Vector3 lastPos;
    private float distanceTraveled;

    public float settleTime = 1f;
    [HideInInspector] public float currentTime = 0f;

    [HideInInspector] public float currentPoints = 0;

    [HideInInspector] public bool doneMoving = false;
    private bool beenMoved = false;
    private bool settled = false;
    private float startWait = 3f;
    public float startTime = 0;

    [Range(1f, 10f)]public float firstForceMultiplier = 1f;



    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
        myBod = GetComponent<Rigidbody>();
        lastPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - startTime > startWait)
        {
            if (transform.position != lastPos)
            {
                if (myBod.velocity.magnitude <= minStopSpeed)
                {
                    if (!settled) { settled = true; lastPos = transform.position; }
                    else if (currentPoints > 0)
                    {
                        if (currentTime < settleTime) { currentTime += Time.deltaTime; }
                        else { doneMoving = true; }
                    }
                }
                else if (settled && !doneMoving)
                {
                    var dist = Vector3.Distance(lastPos, transform.position);
                    distanceTraveled += dist;
                    lastPos = transform.position;

                    if (distanceTraveled > minMoveDistance)
                    {
                        if (!beenMoved)
                        {
                            beenMoved = true; GM.trackPoints(this);
                            if (myBod.velocity.magnitude > GM.impactVelocityTrigger)
                            { StartCoroutine(GM.SlowMo()); }
                            myBod.velocity *= firstForceMultiplier;
                        }

                        currentPoints = myBod.mass * distanceTraveled * pointMultiplier*100;
                        if (currentPoints > maxPoints*100) { currentPoints = maxPoints; doneMoving = true; }
                    }

                    if (dist > minMoveDistance && currentTime > 0 && !doneMoving) { currentTime = 0; }

                }

            }
        } 
        
    }


}
