using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    //how far away can the enemy see
    public float radius;
    //how wide of an angle can the enemy see
    [Range(0, 360)]
    public float angle;

    //reference to player
    public GameObject playerRef;

    //layermasks to determine what the enemy is seeing
    public LayerMask targetMask;
    public LayerMask obstacleMask;

    //can the enemy see the player?
    public bool canSeePlayer; 

    // Start is called before the first frame update
    void Start()
    {
        //assign playerRed
        playerRef = GameObject.FindGameObjectWithTag("Player");
        //start FOV coroutine
        StartCoroutine(FOVRoutine());
    }

    //corountine
    private IEnumerator FOVRoutine()
    {
        //corountine delay time
        WaitForSeconds wait = new WaitForSeconds(0.2f);

        //dont stop coroutine until the object is destroyed
        while (true)
        {
            yield return wait;
            FieldOfViewCheck();
        }
    }


    private void FieldOfViewCheck()
    {
        //collider to look for player  
        Collider[] rangeChecks = Physics.OverlapSphere(transform.position, radius, targetMask);

        //check if collided with anything on the target layer
        if(rangeChecks.Length != 0)
        {
            //the player should be the 1st and only collision on the targetMask layer
            Transform target = rangeChecks[0].transform;

            //direction from where they enemy is facing to where the player is
            Vector3 directionToTarget = (target.position - transform.position).normalized;

            //check the angle from the enemy position to the player position
            if (Vector3.Angle(transform.forward, directionToTarget) < angle / 2)
            {
                //player is within FOV angle

                //how far away is the player
                float distanceToTarget = Vector3.Distance(transform.position, target.position);

                ///check for obstacles in the enemy FOV
                ///using raycast to player
                ///start at center of player, aim at player, stop after reaching the player, stop if it hits an obstacle
                ///! = if NOT hitting obstacles
                if(!Physics.Raycast(transform.position, directionToTarget, distanceToTarget, obstacleMask))
                    canSeePlayer = true;//player is not hidden
                else
                    canSeePlayer = false;//player is hidden by an obstacle
            }
            else
            {
                canSeePlayer = false;//player is not within FOV angle
            }
        }
        else if (canSeePlayer)//if player WAS in view (but not anymore)
        {
            canSeePlayer = false;
        }
    }
}
