using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TP : MonoBehaviour
{
    public TP linkedTo;
    [HideInInspector] public bool tpRecently;
    private bool justLeft = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (justLeft)
        { tpRecently = false; linkedTo.tpRecently = false; justLeft = false; }
    }

    void OnTriggerEnter(Collider col)
    {
        var roller = col.GetComponent<Roll>();

        if (roller && linkedTo && !tpRecently) 
        { 
            tpRecently = true; linkedTo.tpRecently = true;
            roller.transform.position = linkedTo.transform.position;

            var bod = roller.GetComponent<Rigidbody>();
            bod.velocity = linkedTo.transform.forward * bod.velocity.magnitude;
        }
    }

    void OnTriggerExit(Collider col)
    {
        var roller = col.GetComponent<Roll>();

        if (roller && linkedTo && tpRecently) 
        { 
            justLeft = true; 
        }
    }
}
