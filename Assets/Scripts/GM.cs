using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GM : MonoBehaviour
{
    public GameObject pointsPrefab;
    private static GameObject pointPre;
    private static Transform canvasTransform;

    public static int score;

    public static int combo;
    public float comboTime = 1f;
    private static float currentComboTime = 0f;
    private static List<PointTracker> trackingObjects = new List<PointTracker>();
    public static Vector3 pointPosition;
    public AnimationCurve comboScaling;
    private static AnimationCurve comboScale;
    public int maxComboMultiplier = 100;
    private static int maxCombo;
    public static float currentComboPoints;

    public static float sprintPercent;
    public Slider sprintSlider;

    public TextMeshProUGUI[] txtChildren;
    private float comboFontStartSize;
    private int comboPrevSize;

    public float SlowMoTriggerVelocity = 5f;
    public static float impactVelocityTrigger;
    private static bool Slowing = false;

    public float SlowMoDuration = 0.3f;
    private static float SlowMoTime;
    public AnimationCurve SlowSpeedCurve;
    private static AnimationCurve SlowingCurve;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        combo = 0;
        comboPrevSize = 0;

        pointPre = pointsPrefab;
        canvasTransform = transform;

        txtChildren = GetComponentsInChildren<TextMeshProUGUI>();
        if (txtChildren[1]) { comboFontStartSize = txtChildren[1].fontSize; }
        
    }

    // Update is called once per frame
    void Update()
    {
        comboScale = comboScaling;
        maxCombo = maxComboMultiplier;
        impactVelocityTrigger = SlowMoTriggerVelocity;
        SlowMoTime = SlowMoDuration;
        SlowingCurve = SlowSpeedCurve;

        if (txtChildren[0]) { txtChildren[0].text = "Score: " + score; }
        if (txtChildren[1]) 
        { 
            if (combo > 0) 
            {
                txtChildren[1].text = "COMBO *" + combo;
                if (trackingObjects.Count <= 0) 
                { 
                    currentComboTime += Time.deltaTime;
                    if (currentComboTime >= comboTime) { combo = 0; StartCoroutine(TallyScore()); }
                }

                if (comboPrevSize < combo) { StartCoroutine(ComboSizeIncrease()); }
            }
            else { txtChildren[1].text = ""; }
        }
        if (txtChildren[2])
        {
            if (currentComboPoints > 0) { txtChildren[2].text = currentComboPoints + " points"; }
            else { txtChildren[2].text = ""; }
            pointPosition = txtChildren[2].transform.position;        
        }

        if (sprintSlider) { sprintSlider.value = sprintPercent; }
    }

    public IEnumerator ComboSizeIncrease()
    {
        currentComboTime = 0f;
        comboPrevSize = combo;
        
        var sizeIncrease = 1.23f;
        txtChildren[1].fontSize *= sizeIncrease;
        yield return new WaitForSeconds(0.33f);
        txtChildren[1].fontSize = comboFontStartSize;
        
    }

    public static void trackPoints(ObjectPoints obj)
    {
        var tracker = Instantiate(pointPre, canvasTransform).GetComponent<PointTracker>();
        tracker.tracking = obj;
        combo++;

        trackingObjects.Add(tracker);
    }

    public static void instantPoints(int points)
    {
        var tracker = Instantiate(pointPre, canvasTransform).GetComponent<PointTracker>();
        tracker.points = points;
        combo++;
    }

    public static IEnumerator AddScore(PointTracker source, float basePoints)
    {
        int points = (int)source.points;

        while(points > 0)
        {

            currentComboPoints +=100; points -=100;
            
            yield return new WaitForFixedUpdate();
        }

        trackingObjects.Remove(source);
        Destroy(source.gameObject);
    }

    public IEnumerator TallyScore()
    {
        currentComboPoints = (int)(currentComboPoints * (1 + comboScale.Evaluate(combo / maxCombo)));
        
        while(currentComboPoints > 0)
        {
            score +=100; currentComboPoints -=100;
            yield return new WaitForFixedUpdate();;
        }
    }

    public static IEnumerator SlowMo()
    {
        if (!Slowing)
        {
            Slowing = true;

            var currentSlow = 0f;;
            while (currentSlow / 50f < SlowMoTime)
            {
                currentSlow ++;
                yield return new WaitForFixedUpdate();;
                Time.timeScale = SlowingCurve.Evaluate(currentSlow / SlowMoTime);
            }

            Time.timeScale = 1f;
            Slowing = false;
        }
        
    }
}
