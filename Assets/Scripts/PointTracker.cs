using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PointTracker : MonoBehaviour
{
    public Color scoreColor;
    private Color startColor;

    public float blinkTime = 0.2f;
    public float scoreMoveTime = 0.5f;
    private float scoreMoveCurrent = 0f;
    private Vector3 startScoreSpot;

    [HideInInspector] public ObjectPoints tracking;
    [HideInInspector] public int points;

    private TextMeshProUGUI myTxt;
    private IEnumerator blinkCo = null;

    // Start is called before the first frame update
    void Start()
    {
        myTxt = GetComponent<TextMeshProUGUI>();
        startColor = myTxt.color;

        if (!tracking) { StartCoroutine(blinkText()); }
    }

    // Update is called once per frame
    void Update()
    {
        if (scoreMoveCurrent > 0)
        {
            scoreMoveCurrent -= Time.deltaTime;
            transform.position = Vector3.Lerp(GM.pointPosition, startScoreSpot, scoreMoveCurrent / scoreMoveTime);

            if (scoreMoveCurrent <= 0) 
            {
                myTxt.color = Color.clear;
                StartCoroutine(GM.AddScore(this, points));
            }
        }
        else if (tracking)
        {
            transform.position = Camera.main.WorldToScreenPoint(tracking.transform.position);
            points = (int)tracking.currentPoints;

            if (!tracking.doneMoving) 
            {   
                if (tracking.currentTime > 0) { if (blinkCo == null) { blinkCo = blinkText(); StartCoroutine(blinkCo); } }
                else if (blinkCo != null) { StopCoroutine(blinkCo); blinkCo = null; myTxt.color = startColor; }
            }

            if (tracking.currentPoints == tracking.maxPoints && myTxt.color == startColor) 
            { myTxt.color = scoreColor; scoreMoveCurrent = scoreMoveTime; startScoreSpot = transform.position; }

        }

        myTxt.text = points + "";
        
    }

    private IEnumerator blinkText()
    {
        if (tracking)
        {
            while (!tracking.doneMoving)
            {
                if (myTxt.color == startColor) { myTxt.color = scoreColor; }
                else { myTxt.color = startColor; }

                yield return new WaitForSeconds(blinkTime);
            }
        }
        else 
        {
            for (int i = 0; i < 6; i ++)
            {
                if (i % 2 > 0) { myTxt.color = scoreColor; }
                else { myTxt.color = startColor; }

                yield return new WaitForSeconds(blinkTime / 2f);
            }
        }

        myTxt.color = scoreColor;
        scoreMoveCurrent = scoreMoveTime;
        startScoreSpot = transform.position;
        
    }
    
}
