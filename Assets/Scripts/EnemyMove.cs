using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMove : MonoBehaviour
{

    public Transform player;

    public FieldOfView FOV;

    //public List<Transform> waypoints;
    //private int wp = 0;
    //private int oldWP;
    //private int newWP;
    //public Waypoint WP;

    public List<Waypoint> waypoints;
    public int wp;
    private int newWP;

    public NavMeshAgent agent;

    private bool idle;

    //layer mask (only want to look for player)
    //public LayerMask layerMask;

    // Start is called before the first frame update
    void Start()
    {
        //get the navmeshagent component
        agent = GetComponent<NavMeshAgent>();

        

    }

    // Update is called once per frame
    void Update()
    {
        if(FOV.canSeePlayer == true)
        {
            //set agent destination to the players position
            agent.destination = player.position;
        }
        else
        {
            //set agent destination to the next waypoint position
            if(idle == false)
            {
                Move();
            }
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        //if the enemy reached a waypoint
        if (col.gameObject.CompareTag("Waypoint"))
        {
            Debug.Log("WAYPOINT COLLISION");

            //randomized waypoints
            newWP = Random.Range(0, waypoints.Count);
            while (newWP == wp || waypoints[newWP].occupied)
            {
                
                IdleTrue();
                newWP = Random.Range(0, waypoints.Count);
            }
            Invoke("IdleFalse", 3f);
            wp = newWP;




            ///cycling thru waypoints
            //if(wp <= waypoints.Count)
            //{
            //    //move to the next one
            //    wp++;
            //}
            //if(wp == waypoints.Count)
            //{
            //    //go back to beggining
            //    wp = 0;
            //}
        }
    }

    private IEnumerator GenerateWaypointIndex(int wpLength)
    {
        yield return Random.Range(0, wpLength); ;
    }

    private void IdleTrue()
    {
        //play an idle animation
        Debug.Log("IDLE!");
        idle = true;

    }

    private void IdleFalse()
    {
        idle = false;
    }

    private int AssignWaypointIndex(int wpLength)
    {
        return Random.Range(0, wpLength);
    }


    private void Move()
    {
        agent.destination = waypoints[wp].gameObject.transform.position;
    }

    //newWP = Random.Range(0, waypoints.Count);

}
