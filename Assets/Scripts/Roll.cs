using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class Roll : MonoBehaviour
{

    public float acceleration = 4f;
    [Range(0,1)] public float airSpeedModifier = 0.5f;

    public float maxSpeed = 20f;
    public float speedDecayTime = 1f;
    private float speedDecay = 0f;

    public float sprintMulti = 2f;
    public float maxSprintBonus = 10f;

    public float sprintDecayTime = 1f;
    private float sprintDecay = 0f;

    [Range(0f,1f)]public float sprintPercentMin = 0.1f;
    public float sprintTimeTotal = 3f;
    [HideInInspector] public float sprintTimeCurrent;
    public float sprintRecovery = 1f;
    [HideInInspector] public bool sprinting = false;
    [HideInInspector] public bool staminaLocked = false;

    [HideInInspector] public float powerUpSpeedMulti = 1f;
    [HideInInspector] public float powerUpDecay = 1f;
    [HideInInspector] public float powerUpDecayTime = 1f;
    [HideInInspector] public float powerUpActiveTime = 1f;

    public float boostDecayTime = 1f;
    [HideInInspector]public float boostDecay;
    [HideInInspector]public float boostBonus;


    public LayerMask groundMask;
    public float JumpForce = 99;
    [HideInInspector] public Rigidbody myBod;

    private bool grounded;
    [HideInInspector] public bool gliding = false;
    [HideInInspector] public float glideFallSpeed;

    public GameObject slamEffect;
    public float slamDownSpeed = 100f;
    [Range(0f,1f)]public float slamPercentSlow = 0.9f;
    public float slamSpeedMax = 420f;
    private bool slaming = false;

    [Header("FMOD Player SFX")]
    [SerializeField] private FMODUnity.EventReference bubbleSFX; 
    [SerializeField] private FMODUnity.EventReference slamSFX;

    [Header("Player Movement SFX Settings")] 
    [Range(0.1f, 1.5f)] 
    [SerializeField] private float baseBubbleInterval = 0.5f;
    [Range(0f, 30f)]
    [SerializeField] private float speedForFastestBubble = 20f;
    [Range(0.1f, 0.5f)]
    [SerializeField] private float fastestBubbleSpeed = 0.2f;

    [SerializeField] [ReadOnly(true)] private float bubbleSFXCooldown = 0;

    [Header("Player Slam Settings")]
    [SerializeField] private float slamForceForMaxVolume = 20f;

    public GameObject FollowerPrefab;


    // Start is called before the first frame update
    void Start()
    {
        myBod = transform.GetComponent<Rigidbody>();
        sprintTimeCurrent = sprintTimeTotal;

        if (FollowerPrefab) 
        { Instantiate(FollowerPrefab, transform.position, Quaternion.identity).GetComponent<FollowForward>().follow = transform; }
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump")) { Jump(); }

        if (Input.GetButtonDown("Slam")) { Slam(); }

        SprintStamina();
    }

    // Update is called once per frame
    void FixedUpdate()
    {   

        RaycastHit hit;
        grounded = Physics.Raycast(transform.position, Vector3.down, out hit, transform.localScale.y, groundMask);
        Debug.DrawRay(transform.position, Vector3.down);

        if (slaming) 
        {
            if (grounded) { SlamGround(); }
            else
            {
                myBod.velocity *= slamPercentSlow;
                myBod.AddForce(Vector3.down * slamDownSpeed);
                if(myBod.velocity.magnitude > slamSpeedMax)
                { myBod.velocity = Vector3.ClampMagnitude(myBod.velocity, slamSpeedMax); }
                //slamTime += Time.deltaTime;
            }             

        }
        else { Movement(); }

        PlayMovementSFX();
    }

    public Vector3 MoveInput()
    {
        var moveVector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        if (!grounded) { moveVector *= airSpeedModifier; }

        return moveVector.normalized;
    }

    public void Movement()
    {
        var bonusSpeed = 1f;
        if (sprinting)
        { 
            bonusSpeed = sprintMulti;
            sprintDecay = sprintDecayTime;
        }
        else { sprintDecay -= Time.deltaTime; }

        //Debug.Log(Mathf.Lerp(1f, powerUpSpeedMulti, powerUpDecay / powerUpDecayTime));
        bonusSpeed *= Mathf.Lerp(1f, powerUpSpeedMulti, powerUpDecay / powerUpDecayTime);

        if (MoveInput() == Vector3.zero) 
        { if(grounded) { speedDecay -= Time.deltaTime; } }
        else 
        {
            speedDecay = speedDecayTime;
            myBod.AddForce(acceleration * bonusSpeed * MoveInput());
        }

        boostDecay -= Time.deltaTime;
        
        //var baseAir = 0f; if (!grounded) { baseAir = maxSpeed; }
        var baseSpeed = Mathf.Lerp(0f, maxSpeed, speedDecay / speedDecayTime);

        
        var sprintMax = Mathf.Lerp(baseSpeed, baseSpeed + maxSprintBonus, sprintDecay / sprintDecayTime);
        
        sprintMax *= Mathf.Lerp(1f, powerUpSpeedMulti, powerUpDecay / powerUpDecayTime);
        if (powerUpActiveTime > 0f) { powerUpActiveTime -= Time.deltaTime; }
        else if ( powerUpDecay > 0f) { powerUpDecay -= Time.deltaTime; }

        var currentMaxSpeed = Mathf.Lerp(sprintMax, sprintMax + boostBonus, boostDecay / boostDecayTime);
        
        var saveY = myBod.velocity.y;
        myBod.velocity = Vector3.ClampMagnitude(myBod.velocity, currentMaxSpeed);
        
        if (gliding && saveY < 0) { saveY = Mathf.Clamp(saveY, glideFallSpeed, 0f); }
        myBod.velocity = new Vector3(myBod.velocity.x, saveY, myBod.velocity.z);
    }

    public void Jump()
    {
        if (grounded) 
        {
            //speedDecay = speedDecayTime;
            myBod.AddForce(new Vector3(0, JumpForce, 0));
        }   
    }

    public void Slam()
    {
        if (!grounded && !slaming)
        {
            slaming = true;
        }
    }

    public void SlamGround()
    {
        slaming = false;

        // Play slam SFX
        float slamForce = Math.Min(slamForceForMaxVolume, Math.Abs(myBod.velocity.y)) / slamForceForMaxVolume;
        FMODPlayOneShotWithParameter(slamSFX, "Smash Force", slamForce);

        // Slam effect 
        if(slamEffect) 
        { 
            var s = Instantiate(slamEffect, transform.position, Quaternion.identity).GetComponent<SlamScript>();
            s.impactSpeed = myBod.velocity.magnitude;
        }       

    }

    public void SprintStamina()
    {
        GM.sprintPercent = sprintTimeCurrent / sprintTimeTotal;
        if (Input.GetButton("Speed")) 
        {             
            if (GM.sprintPercent >= sprintPercentMin) { sprinting = true; }
        }
        else if (sprinting) { sprinting = false; }

        if (GM.sprintPercent <= 0 && sprinting) { sprinting = false; }

        if (!staminaLocked)
        {
            if (sprinting) { sprintTimeCurrent -= Time.deltaTime; }
            else { sprintTimeCurrent += sprintRecovery * Time.deltaTime; }
            sprintTimeCurrent = Mathf.Clamp(sprintTimeCurrent, 0, sprintTimeTotal);
        }
        
    }

    private void PlayMovementSFX()
    {
        // countdown + leave if not timed out
        bubbleSFXCooldown -= Time.deltaTime;
        if (bubbleSFXCooldown >= 0 || myBod.velocity.magnitude == 0 || !grounded || slaming) return;

        
        // Find and set pitch
        float speedRatio = myBod.velocity.magnitude / speedForFastestBubble;
        float speed = Math.Min(speedRatio, 1);
        FMODPlayOneShotWithParameter(bubbleSFX, "Player Speed", speed);

        // Determine next cooldown
        if (speed < 0.1)
        {
            bubbleSFXCooldown = baseBubbleInterval;
            return;
        }

        bubbleSFXCooldown = ((baseBubbleInterval - fastestBubbleSpeed) * (1 - speed)) + fastestBubbleSpeed;
        //Debug.Log(string.Format("speed: {0}, interval: {1}", speed, bubbleSFXCooldown));
    }

    private void FMODPlayOneShotWithParameter(FMODUnity.EventReference FMODEvent, string param, float value)
    {
        FMOD.Studio.EventInstance instance = FMODUnity.RuntimeManager.CreateInstance(FMODEvent);
        instance.setParameterByName(param, value);
        instance.start();
        instance.release();
    }

    void OnGUI()
    {
        //GUILayout.Box(string.Format("Player speed: {0}", myBod.velocity.magnitude));
    }
}
