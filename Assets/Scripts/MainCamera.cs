using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    private Transform cameraTrans;
    public Transform playerTrans;
    private Roll player;

    public Vector3 camOffset;
    public float lookShift = 1;
    public float lookSpeed = 1;

    private Vector3 newLook;
    private Vector3 lastLook;

    private Vector3 lookOffset;

    private List<GameObject> ditherObjs = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        cameraTrans = gameObject.GetComponent<Transform>();

        if (!playerTrans) { playerTrans = GameObject.Find("Player").transform; }
        if (playerTrans) { player = playerTrans.GetComponent<Roll>(); }  
    }

    // Update is called once per frame
    void Update()
    {
        CameraDither();

        if (playerTrans)
        {
            transform.position = playerTrans.position + camOffset;

            newLook = player.MoveInput() * lookShift;
            lastLook = Vector3.MoveTowards(lastLook, newLook, Time.deltaTime * lookSpeed);
            transform.LookAt(lastLook + playerTrans.position);
        }
        
    }
    /// <summary>
    /// Finds the distance between camera and player then casts a raycast towards the player, if it hits an object with a dithering component it sets the opacity of the texture
    /// </summary>
    private void CameraDither()
    {
        float maxDistance = 10;
        maxDistance = Vector3.Distance(cameraTrans.position, playerTrans.position);

        //Resets the opacity in case its out of range
        foreach (GameObject GO in ditherObjs)
        {
            Dithering dither = GO.GetComponent<Dithering>();
            dither.ResetOpacity();
        }
        Ray ray = new Ray(cameraTrans.position, playerTrans.position - cameraTrans.position);
        RaycastHit[] rayhits = Physics.RaycastAll(ray, maxDistance);
        Debug.DrawRay(cameraTrans.position, ray.direction, Color.cyan);
        foreach (RaycastHit hit in rayhits)
        {
            Dithering dither = hit.collider.gameObject.GetComponent<Dithering>();
            if(dither == null)
            {
               dither = hit.collider.gameObject.GetComponentInParent<Dithering>();
            }
            if (dither != null)
            {
                dither.SetOpacity(hit.distance, maxDistance);
                if (!ditherObjs.Contains(hit.collider.gameObject))
                {
                    ditherObjs.Add(dither.gameObject);
                }
            }
        }
    }

}
