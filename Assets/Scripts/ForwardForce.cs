using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardForce : MonoBehaviour
{
    public float pushForce = 10f;

    public float maxPushSpeed = 50f;

    public bool hitPlayerOnly = false;

    public bool alwaysForward = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerStay(Collider col)
    {
        var bod = col.GetComponent<Rigidbody>();

        if(bod)
        {
            bool canLaunch = true;
            if (hitPlayerOnly && !col.gameObject.CompareTag("Player")) 
            { canLaunch = false; }

            if (canLaunch)
            {
                var dir = transform.forward;
                if (!alwaysForward) { dir = -(transform.position - col.transform.position).normalized; dir.y *= -1; }

                bod.AddForce(pushForce * dir);

                var roller = col.GetComponent<Roll>();
                if (roller) 
                { 
                    roller.boostBonus = maxPushSpeed;
                    roller.boostDecay = roller.boostDecayTime;
                }
                else { bod.velocity = Vector3.ClampMagnitude(bod.velocity, maxPushSpeed); }
            }
            
        }
    }
}
